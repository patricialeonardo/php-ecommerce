CREATE TABLE categories(
    id INT NOT NULL
    AUTO_INCREMENT,
    name VARCHAR(255),
    PRIMARY KEY (id)
);

INSERT INTO categories (name) VALUES ('Suitcases'), ('Bags'), ('Interior Organizers');

CREATE TABLE items (
    id INT NOT NULL
    AUTO_INCREMENT,
    name VARCHAR(255) 
    NOT NULL,
    price INT NOT NULL,
    description VARCHAR(255),
    image VARCHAR(255),
    category_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (category_id)
        REFERENCES categories(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
    );

CREATE TABLE roles (
    id INT NOT NULL
    AUTO_INCREMENT,
    name VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE users(
    id INT NOT NULL
    AUTO_INCREMENT,
    lastName VARCHAR(255)
    NOT NULL,
    firstName VARCHAR(255)
    NOT NULL,
    username VARCHAR(255)
    NOT NULL,
    password VARCHAR(255)
    NOT NULL,
    email VARCHAR(255)
    NOT NULL,
    address VARCHAR(255),
    role_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (role_id)
        REFERENCES roles(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


CREATE TABLE statuses(
    id INT NOT NULL
    AUTO_INCREMENT,
    name VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE payments(
    id INT NOT NULL
    AUTO_INCREMENT,
    name VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE ORDERS(
    id INT NOT NULL AUTO_INCREMENT,
    transaction_code VARCHAR(255),
    purchase_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    total INT,
    user_id INT,
    status_id INT,
    payment_id INT,
    PRIMARY KEY(id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    FOREIGN KEY (status_id)
        REFERENCES statuses(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    FOREIGN KEY (payment_id)
        REFERENCES payments(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE item_order(
    id INT NOT NULL AUTO_INCREMENT,
    quantity INT,
    order_id INT,
    item_id INT,
    PRIMARY KEY(id),
    FOREIGN KEY (order_id)
        REFERENCES orders(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    FOREIGN KEY (item_id)
        REFERENCES items(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

INSERT INTO items (name, price, description, image, category_id) VALUES ("The Carry-On", 225, "The perfect carry-on.", "suitcase1", 1), ("The Weekender", 150, "The perfect weekend bag.", "bag1", 2 ), ("The Dopp Bag", 30, "The perfect organizer", "organizer1", 3);