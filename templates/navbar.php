<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">FIRST CLASS</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor03">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="../views/catalog.php">Catalog</a>
        </li>
    <?php 
      session_start();
      if(isset($_SESSION['user'])&& $_SESSION['user']['role_id']==2){
    ?>
          <li class="nav-item">
            <a class="nav-link" href="../views/cart.php">Cart <span class="badge bg-dark text-light" id="cartCount">
            <?php 
              if(isset($_SESSION['cart'])){
                echo array_sum($_SESSION['cart']);
              }else{
                echo 0;
              }
            ?>
            </span></a>
          </li>
          
    <?php
      }else if(isset($_SESSION['user'])&& $_SESSION['user']['role_id']==1){
    ?>
          <li class="nav-item">
            <a class="nav-link" href="../views/add_item_form.php">Add Product</a>
          </li>
    <?php
      };
      
      if(isset($_SESSION['user'])){
    ?>
          <li class="nav-item">
            <a class="nav-link" href="">Hello, <?php echo $_SESSION['user']['username'] ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../controllers/process_logout.php">Logout</a>
          </li>
    <?php
      }else{
    ?>
          <li class="nav-item">
            <a class="nav-link" href="../views/register.php">Register</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../views/login.php">Login</a>
          </li>
    <?php
      };
    ?>
  
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>