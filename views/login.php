<?php
   require("../templates/template.php");
   function get_content() {
   ?>
       <h1 class="text-center py-5">Login</h1>
       <div class="container">
               <div class="col-lg-4 offset-lg-4">
                   <form action="" method="POST">
                       <div class="form-group">
                           <label for="email">Email</label>
                           <input id="email" type="email" name="email" class="form-control">
                           <span></span>
                       </div>
                       <div class="form-group">
                           <label for="password">Password</label>
                           <input id="password" type="password" name="password" class="form-control">
                       </div>
                   </form>
                   <button class="btn btn-info" id="loginUser" type="button">Login</button>
               </div>
                   <p class="text-center">
                       Not yet registered?
                       <a href="register.php">Register</a>
                   </p>
       </div>
       <script src="../assets/scripts/login.js"></script>
   <?php
   }
?>