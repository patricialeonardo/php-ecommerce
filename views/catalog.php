<?php
    require "../templates/template.php";
    function get_content(){
        require "../controllers/connection.php";
        ?>
            
            <div class="container">
                <div class="row">
                    <!-- Sidebar -->
                    <div class="col-lg-2">
                        <h3>Categories</h3>
                        <ul class="list-group border">
                            <li class="list-group-item">
                                <a href="catalog.php">All</a>
                            </li>
                            <?php
                                $categories_query = "SELECT * FROM categories";
                                $categoryList = mysqli_query($conn, $categories_query);
                                foreach($categoryList as $indiv_category){
                                    ?>
                                    <li class="list-group-item">
                                        <a href="catalog.php?category_id=<?php echo $indiv_category['id']?>">
                                            <?php echo $indiv_category['name']?></a>
                                    </li>
                                    <?php
                                }
                             ?>
                        </ul>
                        <!-- Sorting -->
                        <h3>Sort By</h3>
                        <ul class="list-group border">
                            <li class="list-group-item">
                                <a href="../controllers/process_sort.php?sort=asc">Price (Lowest to Highest)</a>
                            </li>
                            <li class="list-group-item">
                                <a href="../controllers/process_sort.php?sort=desc">Price (Highest to Lowest)</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Item list -->
                    <div class="col-lg-10">
                    <h1 class="text-center py-5">CATALOG PAGE</h1>
                        <div class="row">
                            <?php
                            //review this for later
                            // session_start();
                                //steps for retrieving item
                                $items_query = "SELECT * FROM items";
                                //filtering
                                if(isset($_GET['category_id'])){
                                    $catId = $_GET['category_id'];
                                    $items_query .= " WHERE category_id = $catId";
                                }
                                //sort
                                if(isset($_SESSION['sort'])){
                                    $items_query .= $_SESSION['sort'];
                                }
                                //mysqli($connection variable from connection.php, )
                                $items = mysqli_query($conn, $items_query);
                                foreach ($items as $indiv_item){
                                    ?>
                                        <div class="col-lg-4 py-2">
                                            <div class="card h-100">
                                                <img class="card-img-top" src="<?php echo $indiv_item['image'] ?>" alt="image">
                                                <div class="card-body">
                                                    <h4 class="card-title"><?php echo $indiv_item['name']?></h4>
                                                    <p class="card-text">$<?php echo $indiv_item['price']?>.00 USD</p>
                                                    <p class="card-text"><?php echo $indiv_item['description']?></p>
                                                    <?php
                                                            //process of displaying category
                                                        //1. get category name where id = $indiv_item['category_id']
                                                        //2. display the data
                                                        $catId = $indiv_item['category_id'];
                                                        $category_query = "SELECT * FROM categories WHERE id = $catId";
                                                        $category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
                                                        // foreach($category as $key => $value){
                                                            ?>
                                                                <p class="card-text"><?php echo $category['name']?></p>
                                                            <?php
                                                        
                                                     ?>
                                                    
                                                </div>
                                                <?php 
                                                    if(isset($_SESSION['user'])&& $_SESSION['user']['role_id']==1){
                                                        ?>
                                                    <div class="card-footer">
                                                    <a href="edit_item_form.php?id=<?php echo $indiv_item['id']?>" class="btn btn-secondary">Edit Item</a>
                                                    <a href="../controllers/process_delete_item.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-danger">Delete Item</a>
                                                    </div>
                                                    <?php
                                                    }else{
                                                    ?>
                                                        <div class="card-footer">
                                                        <form action="../controllers/process_update_cart.php" method="POST">
                                                            <input type="number" class="form-control" value="1" name="quantity">
                                                            <input type="hidden" name="id" value="<?php echo $indiv_item['id']?> ">
                                                            <button type="submit" class= "btn btn-primary">Add To Cart</button>
                                                        </form>
                                                        <input type="number" class="form-control" value="1">
                                                        <button type="button" class="btn btn-success addToCartBtn" data-id="<?php echo $indiv_item['id'] ?>">Add To Cart</button>
                                                    </div>
                                                    <?php
                                                    }
                                                ?>
                                              
                                                
                                            </div>
                                        </div>
                                    <?php
                                }
                             ?>
                        </div>
                    </div>
                </div>
            </div>
            <script type ="text/javascript" src="../assets/scripts/addToCart.js"></script>
        <?php
    }    
?>

