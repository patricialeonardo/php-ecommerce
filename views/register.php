<?php 
	require "../templates/template.php";
	function get_content(){

?>
	<h1 class="text-center py-4">Register</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form action="../controllers/process_register_user.php" method="POST">
					<!-- First Name -->
					<div class="form-group">
					<label for="firstName">First Name</label>
					<input 
						type="text" 
						name="firstName" 
						class="form-control"
						id="firstName">
						<span class="validation"></span>
					<!-- Last Name -->
					<div class="form-group">
					<label for="lastName">Last Name</label>
					<input 
						type="text" 
						name="lastName" 
						class="form-control"
						id="lastName">
						<span class="validation"></span>
					</div>
					<!-- email -->
					<div class="form-group">
						<label for="email">Email:</label>
						<input 
							type="email" 
							name="email"
							class="form-control"
							id="email">
						<span class="validation"></span>

					</div>
					<!-- address -->
					<div class="form-group">
						<label for="address">Address:</label>
						<input 
							type="text" 
							name="address"
							class="form-control"
							id="address">
						<span class="validation"></span>

					</div>
					<!-- username -->
					<div class="form-group">
						<label for="username">Username</label>
						<input 
							type="text" 
							name="username"
							class="form-control"
							id="username">
						<span class="validation"></span>

					</div>
					<!-- password -->
					<div class="form-group">
						<label for="password">Password</label>
						<input 
							type="password" 
							name="password"
							class="form-control"
							id="password">
						<span class="validation"></span>

					</div>
					<!-- confirm password -->
					<div class="form-group">
						<label for="confirm">Confirm Password</label>
						<input 
							type="password" 
							name="confirm"
							class="form-control"
							id="confirm">
						<span class="validation"></span>
					</div>
				</form>
				<button class="btn btn-primary" type="submit" id="registerUser">Register</button>
				
				<p>Already Registered? <a href="login.php">Login</a></p>

			</div>
		</div>
	</div>
	<script scr="../assets/scripts/register.js"></script>
<?php
	}

?>