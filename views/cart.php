<?php 
    require "../templates/template.php";
    function get_content(){
        require "../controllers/connection.php";
        //REVIEW THIS LATER
        // session_start();
?>
    <h1 class=text-center py-5>CART</h1>
        <hr>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr class="text-center">
                        <th>Item</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Subtotal</th>
                        <th></th>
                    </tr>
                            </thead>
                            <tbody>
                                <?php
						        $total =0;
                                    //check if session exists
                                    if(isset($_SESSION['cart'])){
                                       foreach($_SESSION['cart'] as $item_id => $item_quantity){$item_query = "SELECT * FROM items WHERE id = $item_id"; 
                                        
                                        $indiv_item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));

                                        $subtotal = $indiv_item['price']*$item_quantity;

                                        $total += $subtotal;
                                    ?>
                                    <tr>
                                        <td> <?php echo $indiv_item['name']?></td>
                                        <td> $<?php echo $indiv_item['price']?>.00</td>
                                        <td> 
                                        <form action="../controllers/process_update_cart.php" method="POST">
                                            <input type="hidden" name="fromCartPage" value="true">
                                            <input type="hidden" name="id" value="<?php echo $item_id ?>">
                                            <input type="number" name="quantity" value="<?php echo $item_quantity ?>" class="form-control quantityInput">
                                        </form>
                                        
                                        </td>
                                        <td> $<?php echo $subtotal?>.00</td>
                                        <td><a href="../controllers/process_remove_item.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-warning">Remove Item</a></td>
                                    </tr>
                                    <?php
                                    }
                                    } 
                                         
                                //Steps
                                //1. Check whether we have $_SESSION CART
                                //2. Get the session cart and display each in a <tr>
                                    //a. Check the data type of our $_SESSION['cart']
                                    //b. Do the loop (foreach($data as %key => $value))
                                    //c. The goal is to get the id and the quantity
                                    //d. Get the item details through item_query
                                    //e. Select * FROM items WHERE id = the id we got from the foreach
                                    //f. use mysqli_query to get individual item
                                    //g. get the subtotal by multiplying indiv_item['price'] and quantiy we got from the foreach
                                    //h. get the total  (in order for us to do so, we must intitialize $total at the very top)
                                    //i. display the details in <tr><td>
                                ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><a href="../controllers/process_empty_cart.php" class="btn btn-danger">Empty Cart</a></td>
                                    <td>Total: $ <?php echo $total?>.00</td>
                                    <td><a href="../controllers/process_empty_cart.php" class="btn btn-primary">Pay via COD</a></td>

                                </tr>
                            </tbody>
            </table>    
        </div>

<script src="../assets/scripts/editCartItem.js"></script>
<?php
    }
?>