<?php 
    require "connection.php";


    function validate_form(){
        $errors = 0;
        $file_types = ["jpg", "jpeg", "png", "gif", "bmp", "sbvg"];
        $file_ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));

        if($_POST['name']=="" || ! isset ($_POST['name'])){$errors++; 
        };

        if($_POST['price']<=0 || ! isset($_POST['price'])){$errors++;
        };

        if($_POST['description'] == "" || !isset ($_POST['description'])){$errors++;
        };

        if($_POST['category_id']=="" || !isset ($_POST['category_id'])){$errors++;
        };

        if($_FILES['image'] == "" || !isset ($_FILES['image'])){$errors++;
        };

        if (!in_array($file_ext, $file_types)){$errors++;
        };

        if($errors>0){
            return false;
        }else{
            return true;
        }
        // validation logic
        // we'll check if each of the fields in the form has a value. if not, it will increase the $error total and if the $error total > 0 it will return false
        //we'll check if the file extension of the image is within the acceptable file extensions. if not, it will increase the $error total and if the $error total > 0 it will return false.

    };

    if(validate_form()){

        $name = $_POST['name'];
        $price = $_POST['price'];
        $description = $_POST['description'];
        $category_id = $_POST['category_id'];

        $destination = "../assets/images/";
        $file_name = $_FILES['image']['name'];

        move_uploaded_file($_FILES['image']['tmp_name'], $destination.$file_name);
        //PROCESS OF SAVING AN ITEM
        //1. CAPTURE ALL DAT AFROM FORM THROUGH $_POST OR $_FILES FOR IMAGE
        //2. MOVE UPLOADED IMAGE FILE TO TEH ASSETS/IMAGES DIRECTORY
        //3. CREATE THE QUERY
        //4. USE MYSLQI_QUERY 
        //5. GO BACK TO CATALOG IF SUCCESSFUL
        //6. IF UNSUCCESSFUL GO BACK TO ADD_ITEM_FORM

        $image = $destination . $file_name;

        $add_item_query = "INSERT INTO items (name, price, description, category_id, image) VALUES ('$name', $price, '$description', $category_id, '$image')";

        $new_item = mysqli_query($conn, $add_item_query);
         

        header("Location: ../views/catalog.php");
    }else{
        header_remove("Location: ". $_SERVER['HTTP_REFERER']);
    };
?>