<?php 
    $host = "localhost";
    $db_username = "root";
    $db_password = "";
    $db_name = "b46ecommerce";

    $conn = mysqli_connect($host, $db_username, $db_password, $db_name);

    if(!$conn){
        die("Connection Failed: " . mysqli_error($conn));
    }
?>